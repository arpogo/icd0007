<?php
$connection = new PDO("sqlite:db.sqlite");
$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if (!file_exists('installed')) {
    $create_contacts_table = $connection->prepare("
        CREATE TABLE contacts (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            firstName VARCHAR(255),
            lastName VARCHAR(255)
        )
    ");
    $contacts_result = $create_contacts_table->execute();
    $create_phones_table = $connection->prepare("
        CREATE TABLE phones(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            contact_id UNSIGNED INTEGER,
            number VARCHAR(255)
        )
    ");
    $phones_result = $create_phones_table->execute();
    if ($contacts_result && $phones_result) {
        file_put_contents('installed', '');
    } else {
        die('Something went wrong with setting up the tables!');
    }
}

?>